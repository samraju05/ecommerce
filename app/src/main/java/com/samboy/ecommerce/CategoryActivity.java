package com.samboy.ecommerce;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.samboy.ecommerce.adapter.AdapterCategory;
import com.samboy.ecommerce.app.App;
import com.samboy.ecommerce.constant.IC;
import com.samboy.ecommerce.constant.SC;
import com.samboy.ecommerce.database.Database;
import com.samboy.ecommerce.interfaces.CategoryInterface;
import com.samboy.ecommerce.model.Category;
import com.samboy.ecommerce.utils.DummyArrays;
import com.samboy.ecommerce.utils.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CategoryActivity extends BaseActivity implements CategoryInterface {
    private Toolbar mToolbar;
    private Database db;
    private RecyclerView rvCate;
    private AdapterCategory mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        db=Database.getInstance(this);
        setUpToolBar();
        intViews();
        setRvCategory();

    }
    public void setUpToolBar() {
        mToolbar = findViewById(R.id.toolbarCategory);
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_home_back);
        App.changeToolbarFont(mToolbar);
        actionBar.setTitle("Category");
    }
    private void intViews(){
        rvCate=findViewById(R.id.rvCategory);
    }
    private void setRvCategory(){
        mAdapter=new AdapterCategory(this,this, DummyArrays.getCategory());
        rvCate.setLayoutManager(Utils.getLyMgr(1,IC.VERTICAL));
        rvCate.setAdapter(mAdapter);
    }

    @Override
    public void onBackPressed() {
        if (!isFinishing()){
            finish();
        }
    }

    @Override
    public void onCategoryClick(Category category) {
        db.addSelectedCategory(category);
        if (!isFinishing()){
            finish();
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (!isFinishing()) {
                    finish();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}

package com.samboy.ecommerce.constant;

public class IC {
    public static final int ZERO=0;
    public static final int HORIZONTAL=0;
    public static final int VERTICAL=1;

    public static final int TO_STORE=101;
    public static final int FINISH_STORE=102;
}

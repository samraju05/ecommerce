package com.samboy.ecommerce.login;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.samboy.ecommerce.BaseActivity;
import com.samboy.ecommerce.R;
import com.samboy.ecommerce.app.App;
import com.samboy.ecommerce.utils.Validation;

public class LoginActivity extends BaseActivity implements View.OnClickListener {
    private Toolbar mToolbar;
    private EditText edtEmail;
    private EditText edtPassword;
    private TextView txtForgot;
    private TextView txtSignup;
    private Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setUpToolBar();
        intViews();
        setListener();
    }public void setUpToolBar() {
        mToolbar = findViewById(R.id.toolbarLogin);
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_home_back);
        actionBar.setTitle("Login");
        App.changeToolbarFont(mToolbar);
    }

    private void intViews() {
        edtEmail = findViewById(R.id.edtLoginEmail);
        edtPassword = findViewById(R.id.edtLoginPassword);
        txtForgot = findViewById(R.id.txtLoginForgot);
        txtSignup = findViewById(R.id.txtLoginSignUp);
    }

    private void setListener() {
        btnLogin.setOnClickListener(this);
        txtForgot.setOnClickListener(this);
        txtSignup.setOnClickListener(this);
    }
    private void login(){
        if (Validation.checkIsAnEmail(edtEmail)){
            if (!Validation.checkIsEmpty(edtPassword)){
                finish();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnLogin:
                login();
                break;
            case R.id.txtLoginForgot:
                startActivity(new Intent(this, ForgotPasswordActivity.class));
                break;
            case R.id.txtLoginSignUp:
                startActivity(new Intent(this, RegisterActivity.class));
                break;
        }
    }
}

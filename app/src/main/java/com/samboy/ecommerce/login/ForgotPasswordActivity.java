package com.samboy.ecommerce.login;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.samboy.ecommerce.BaseActivity;
import com.samboy.ecommerce.R;
import com.samboy.ecommerce.app.App;
import com.samboy.ecommerce.utils.Validation;

public class ForgotPasswordActivity extends BaseActivity implements View.OnClickListener {
    private Toolbar mToolbar;
    private EditText edtEmail;
    private Button btnForgot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_passsword);
        setUpToolBar();
        intVirews();
        setListener();
    }
    public void setUpToolBar() {
        mToolbar = findViewById(R.id.toolbarForgot);
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_home_back);
        actionBar.setTitle("Forgot Password");
        App.changeToolbarFont(mToolbar);
    }

    private void intVirews() {
        edtEmail = findViewById(R.id.edtForgotEmail);
        btnForgot = findViewById(R.id.btnForgot);
    }

    private void setListener() {
        btnForgot.setOnClickListener(this);
    }

    private void forgot() {
        if (!Validation.checkIsEmpty(edtEmail)) {

        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnForgot:
                forgot();
                break;
        }
    }
}

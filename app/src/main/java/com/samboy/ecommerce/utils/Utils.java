package com.samboy.ecommerce.utils;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Base64;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.samboy.ecommerce.app.App;
import com.samboy.ecommerce.model.Cart;

import java.io.File;
import java.io.FileInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

/**
 * Created by Hari Group Unity on 01-02-2018.
 */

public class Utils {

    public static StaggeredGridLayoutManager getLyMgr(int spnCount,int orientation){
        return new StaggeredGridLayoutManager(spnCount,orientation);
    }

    public static<T> ArrayAdapter<T> getSpnAdapter(Context context, List<T> mList){
        ArrayAdapter<T> mAdapter=new ArrayAdapter<T>(context, android.R.layout.simple_list_item_1, mList){
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                App.setTypeface((TextView) v, null);
                return v;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getDropDownView(position, convertView, parent);
                App.setTypeface((TextView) v, null);
                return v;
            }
        };
        return mAdapter;
    }

    public static int getPrice(Cart cart){
        String p=cart.getOfferPrice().replace("₹","");
        return Integer.parseInt(p)*cart.getCount();
    }


    public static String getTwoLetter(String name) {
        String f, s, fin;
        int indexF = 0, indexS;

        if (name.contains(" ")) {
            indexS = 1 + (name.indexOf(" "));
            f = name.substring(indexF, indexF + 1);
            s = name.substring(indexS, indexS + 1);
            fin = (f + "" + s).toUpperCase();
            return fin;
        } else {
            f = name.substring(indexF, indexF + 1);
            return f;
        }

    }

    public static String getFirstLtr(String name) {
        String var="";
        if (!name.isEmpty()){
            var= name.substring(0,1);
        }
        return var;
    }


    public static String getFileName(String filePath) {
        String filename = filePath.substring(filePath.lastIndexOf("/") + 1);
        return removeExtension(filename);
    }

    public static String removeExtension(String filename) {
        if (filename == null) {
            return null;
        }
        int index = indexOfExtension(filename);
        if (index == -1) {
            return filename;
        } else {
            return filename.substring(0, index);
        }
    }

    public static String getExtension(String filename) {
        if (filename == null) {
            return null;
        }

        int index = indexOfExtension(filename);

        if (index == -1) {
            return filename;
        } else {
            return filename.substring(index);
        }
    }

    private static final char EXTENSION_SEPARATOR = '.';
    private static final char DIRECTORY_SEPARATOR = '/';

    public static int indexOfExtension(String filename) {

        if (filename == null) {
            return -1;
        }

        // Check that no directory separator appears after the
        // EXTENSION_SEPARATOR
        int extensionPos = filename.lastIndexOf(EXTENSION_SEPARATOR);

        int lastDirSeparator = filename.lastIndexOf(DIRECTORY_SEPARATOR);

        if (lastDirSeparator > extensionPos) {
            return -1;
        }

        return extensionPos;
    }

    public static String getDateFormat(int month, int day, int year) {
        String finalDate;
        String date = String.format("%02d%02d%02d", month, day, year);
        finalDate = String.format("%s-%s-%s", date.substring(0, 2),
                date.substring(2, 4),
                date.substring(4, 8));
        return finalDate;
    }

    public static String getTimeFormat(int hour, int minut) {
        String finalTime;
        String time = String.format("%02d%02d", hour, minut);
        finalTime = String.format("%s:%s", time.substring(0, 2),
                time.substring(2, 4));
        return finalTime;
    }

    public static String milliSecondsToTimer(long milliseconds) {
        String finalTimerString = "";
        String secondsString = "";

        // Convert total duration into time
        int hours = (int) (milliseconds / (1000 * 60 * 60));
        int minutes = (int) (milliseconds % (1000 * 60 * 60)) / (1000 * 60);
        int seconds = (int) ((milliseconds % (1000 * 60 * 60)) % (1000 * 60) / 1000);
        // Add hours if there
        if (hours > 0) {
            finalTimerString = hours + ":";
        }
        // Prepending 0 to seconds if it is one digit
        if (seconds < 10) {
            secondsString = "0" + seconds;
        } else {
            secondsString = "" + seconds;
        }
        finalTimerString = finalTimerString + minutes + ":" + secondsString;

        // return timer string
        return finalTimerString;
    }

    public static String md5(String s) {
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest.getInstance("MD5");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++)
                hexString.append(Integer.toHexString(0xFF & messageDigest[i]));

            return hexString.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static void deleteCache(Context context) {
        try {
            File dir = context.getCacheDir();
            deleteDir(dir);
        } catch (Exception e) {
        }
    }

    public static boolean haveNetworkConnection(Context context) {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }

    private static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
            return dir.delete();
        } else if (dir != null && dir.isFile()) {
            return dir.delete();
        } else {
            return false;
        }
    }

    public static String getPath(Context context, Uri uri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(uri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }


    public static void hideKeyBoard(Activity activity) {
        hideKeyBoard(activity.getCurrentFocus());

    }

    public static void hideKeyBoard(View view) {

        if (view != null) {
            // view.clearFocus();
            InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }



    public static String fileToString(String filePath) {
        File file = new File(filePath);
        if (file.exists() && file.canRead()) {
            try {
                return Base64.encodeToString(getFileBytes(filePath), Base64.DEFAULT);
            } catch (Exception e) {

            }
        }
        return "";
    }

    public static byte[] getFileBytes(String path) {
        FileInputStream fis = null;
        byte[] by = null;
        try {
            fis = new FileInputStream(path);
            by = new byte[fis.available()];
            fis.read(by);
        } catch (Exception e) {

        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (Exception e) {
                }
            }
        }
        return by;
    }


    public static String getRealPathFromURI(Context context, Uri contentUri) {

        Cursor cursor = null;
        try {
            if (contentUri.getPath() != null && new File(contentUri.getPath()).exists()) {
                return contentUri.getPath();
            }
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, null, null, null, null);


            int column_index = cursor.getColumnIndex(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();

            if (column_index == -1) {
                try {
                    String path=getPhoneGalleryImages(context,cursor.getString(cursor.getColumnIndex("document_id")).substring("image:".length()));
                    if(path!=null) {
                        return path;
                    }
                } catch (Exception e) {
                }
            }
            return cursor.getString(column_index);
        } catch (Exception e) {
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return null;
    }

    private static String getPhoneGalleryImages(Context context, String imageId) {

        Cursor cursor = context.getContentResolver().query(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, null, MediaStore.Images.Thumbnails._ID + "=?", new String[]{imageId},
                null, null);
        String path = null;
        if (cursor != null && cursor.moveToFirst()) {
            path = cursor.getString(cursor
                    .getColumnIndex(MediaStore.Images.Media.DATA));

        }
        if (cursor != null)
            cursor.close();

        return path;
    }



}

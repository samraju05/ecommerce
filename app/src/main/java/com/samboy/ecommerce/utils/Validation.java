package com.samboy.ecommerce.utils;

import android.util.Patterns;
import android.widget.EditText;

import com.samboy.ecommerce.R;

import java.util.regex.Pattern;

/**
 * Created by Hari Group Unity on 16-02-2018.
 */

public class Validation {

    private static final Pattern specialChars = Pattern.compile("[$=\\\\|/<>^*%]");


    public static boolean checkIsEmpty(EditText... editTexts) {
        boolean isEmpty = false;

        for (EditText e : editTexts) {
            if (e.getText().toString().trim().isEmpty()) {
                isEmpty = true;
                e.setError(e.getResources().getString(R.string.err_empty));
            } else {
                e.setError(null);
            }
        }
        return isEmpty;
    }

    public static boolean checkIsAnEmail(EditText editText) {
        if(Patterns.EMAIL_ADDRESS.matcher(editText.getText().toString()).matches()) {
            editText.setError(null);
            return true;
        }
        editText.setError(editText.getResources().getString(R.string.err_email_valid));
        return false;
    }

    public static boolean isAValidMobile(EditText editText) {
        if(Patterns.PHONE.matcher(editText.getText().toString()).matches()) {
            editText.setError(null);
            return true;
        }
        editText.setError(editText.getResources().getString(R.string.err_mobile_valid));
        return false;
    }

    public static boolean checkIsHavingSpecialChars(EditText... editTexts) {
        boolean contains = false;
        for (EditText e : editTexts) {
            if(specialChars.matcher(e.getText().toString()).find()) {
                contains=true;
                e.setError(e.getResources().getString(R.string.err_special_characters));
            }
        }
        return contains;
    }

    public static boolean isAValidPassword(EditText... editText) {
        if (!checkIsEmpty(editText) && !checkIsHavingSpecialChars(editText)) {
            for (EditText ed : editText) {
                String text = ed.getText().toString();
                if (text.contains(" ") || text.contains("\t")) {
                    ed.setError(ed.getResources().getString(R.string.err_pass_space));
                    return false;
                }
                if (text.length() < 4 || text.length() > 32) {
                    ed.setError(ed.getResources().getString(R.string.err_pass_length));
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    public static boolean isAvalidName(EditText editText) {
        boolean isValid=editText.getText().toString().matches("[a-zA-Z0-9 ]*");
        if(!isValid) {
            editText.setError(editText.getResources().getString(R.string.err_special_characters));
        } else {
            editText.setError(null);
        }
        return isValid;
    }
}

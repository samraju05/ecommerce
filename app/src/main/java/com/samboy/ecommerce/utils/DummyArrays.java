package com.samboy.ecommerce.utils;

import com.samboy.ecommerce.model.Category;
import com.samboy.ecommerce.model.Product;
import com.samboy.ecommerce.model.Store;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class DummyArrays {

    private static String imageId = "2131230844";

    public static List<Store> getStore() {
        List<Store> mList = new ArrayList<>();
        Store sm = new Store(1, "Store 1");
        Store sm1 = new Store(1, "Store 2");
        Store sm2 = new Store(1, "Store 3");
        Store sm3 = new Store(1, "Store 4");
        mList.add(sm);
        mList.add(sm1);
        mList.add(sm2);
        mList.add(sm3);
        return mList;
    }

    public static List<Category> getCategory() {
        List<Category> mList = new ArrayList<>();
        Category c1 = new Category(1, "All", imageId);
        Category c2 = new Category(2, "Fruits", imageId);
        Category c3 = new Category(3, "Veg", imageId);
        Category c4 = new Category(4, "Bathroom", imageId);
        Category c5 = new Category(5, "Makeup", imageId);
        Category c6 = new Category(6, "Others", imageId);
        mList.add(c1);
        mList.add(c2);
        mList.add(c3);
        mList.add(c4);
        mList.add(c5);
        mList.add(c6);
        return mList;

    }

    public static List<Product> getProducts(int cateId) {
        switch (cateId) {
            case 1:
                return getAll();
            case 2:
                return getFruits();
            case 3:
                return getVeg();
            case 4:
                return getBathroom();
            case 5:
                return getMakeups();
            case 6:
                return getOthers();
            default:
                return getAll();
        }
    }

    public static List<Product> getAll() {
        List<Product> mList = new ArrayList<>();
        mList.addAll(getFruits());
        mList.addAll(getVeg());
        mList.addAll(getBathroom());
        mList.addAll(getMakeups());
        mList.addAll(getOthers());
        return mList;
    }

    public static List<Product> getFruits() {
        List<Product> mList = new ArrayList<>();
        Product p1 = new Product(1, "Fruty", imageId, "₹500", "₹255");
        Product p2 = new Product(2, "Juice", imageId, "₹500", "₹255");
        Product p3 = new Product(3, "Appy Fiz", imageId, "₹500", "₹255");
        Product p4 = new Product(4, "Falooda", imageId, "₹500", "₹255");
        Product p5 = new Product(5, "Fruits", imageId, "₹500", "₹255");
        mList.add(p1);
        mList.add(p2);
        mList.add(p3);
        mList.add(p4);
        mList.add(p5);
        return mList;
    }

    public static List<Product> getVeg() {
        List<Product> mList = new ArrayList<>();
        Product p1 = new Product(6, "Chakka", imageId, "₹500", "₹255");
        Product p2 = new Product(7, "Thanga", imageId, "₹500", "₹255");
        Product p3 = new Product(8, "Manga", imageId, "₹500", "₹255");
        mList.add(p1);
        mList.add(p2);
        mList.add(p3);
        return mList;
    }

    public static List<Product> getBathroom() {
        List<Product> mList = new ArrayList<>();
        Product p1 = new Product(9, "Shampoo", imageId, "₹500", "₹255");
        Product p2 = new Product(10, "Brush", imageId, "₹500", "₹255");
        Product p3 = new Product(11, "Comb", imageId, "₹500", "₹255");
        Product p4 = new Product(12, "Powder", imageId, "₹500", "₹255");
        Product p5 = new Product(13, "Axe", imageId, "₹500", "₹255");
        mList.add(p1);
        mList.add(p2);
        mList.add(p3);
        mList.add(p4);
        mList.add(p5);
        return mList;
    }

    public static List<Product> getMakeups() {
        List<Product> mList = new ArrayList<>();
        Product p1 = new Product(16, "Rice", imageId, "₹500", "₹255");
        Product p2 = new Product(17, "Rice", imageId, "₹500", "₹255");
        Product p3 = new Product(18, "Rice", imageId, "₹500", "₹255");
        mList.add(p1);
        mList.add(p2);
        mList.add(p3);
        return mList;
    }

    public static List<Product> getOthers() {
        List<Product> mList = new ArrayList<>();
        Product p1 = new Product(19, "Rice", imageId, "₹500", "₹255");
        Product p2 = new Product(20, "Oil", imageId, "₹500", "₹255");
        Product p3 = new Product(21, "Sugar", imageId, "₹500", "₹255");
        mList.add(p1);
        mList.add(p2);
        mList.add(p3);
        return mList;
    }
}

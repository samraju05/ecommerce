package com.samboy.ecommerce.location;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.samboy.ecommerce.BaseActivity;
import com.samboy.ecommerce.R;
import com.samboy.ecommerce.constant.IC;
import com.samboy.ecommerce.database.Database;
import com.samboy.ecommerce.constant.SC;

public class SelectLocation extends BaseActivity implements View.OnClickListener {
    private Button btnChoose;
    private ImageView imgChooseLocation;
    private Database db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_location);
        db=Database.getInstance(this);
        if (db.isSelectedStore()){
            finish();
        }
        intViews();
        setListener();
        setImage();
    }

    private void intViews() {
        imgChooseLocation = findViewById(R.id.imgChooseLocation);
        btnChoose=findViewById(R.id.btnChoose);

    }

    private void setListener() {
        btnChoose.setOnClickListener(this);
    }

    private void setImage() {
        Glide.with(this)
                .load(R.drawable.locationimg)
                .into(imgChooseLocation);
    }

    @Override
    protected void onResume() {
        if (db.isSelectedStore()){
            finish();
        }
        super.onResume();

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnChoose:
                Intent in=new Intent(this,SelectStore.class);
                in.putExtra(SC.ACTIVITY, IC.TO_STORE);
                startActivity(in);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        doublClickExit();
    }
}

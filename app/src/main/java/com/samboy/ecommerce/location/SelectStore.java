package com.samboy.ecommerce.location;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.View;

import com.samboy.ecommerce.BaseActivity;
import com.samboy.ecommerce.HomeActivity;
import com.samboy.ecommerce.R;
import com.samboy.ecommerce.adapter.AdapterStore;
import com.samboy.ecommerce.constant.IC;
import com.samboy.ecommerce.database.Database;
import com.samboy.ecommerce.interfaces.StoreSelect;
import com.samboy.ecommerce.model.Store;
import com.samboy.ecommerce.utils.DummyArrays;
import com.samboy.ecommerce.constant.SC;

public class SelectStore extends BaseActivity implements View.OnClickListener, StoreSelect {

    private RecyclerView rvStore;
    private AdapterStore mAdapter;
    private Database db;
    private int recId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_store);
        db = Database.getInstance(this);
        recId = getIntent().getIntExtra(SC.ACTIVITY, 0);
        intviews();
        setListener();
        setRvStore();
    }

    private void intviews() {
        rvStore = findViewById(R.id.rvStore);
    }

    private void setListener() {
    }

    private void setRvStore() {
        mAdapter = new AdapterStore(DummyArrays.getStore(), this);
        StaggeredGridLayoutManager stg = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        rvStore.setLayoutManager(stg);
        rvStore.setAdapter(mAdapter);
    }

    private void gotoAct(int id) {
        switch (id) {
            case IC.TO_STORE:
                if (!isFinishing()) {
                    startActivity(new Intent(this, HomeActivity.class));
                    finish();
                }
                break;
            case IC.FINISH_STORE:
                finish();
                break;
        }
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void onStoreSelect(Store storeModel) {
        db.addSelectedStore(storeModel);
        gotoAct(recId);
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}

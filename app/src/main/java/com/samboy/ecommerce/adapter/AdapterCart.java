package com.samboy.ecommerce.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.samboy.ecommerce.R;
import com.samboy.ecommerce.model.Cart;
import com.samboy.ecommerce.utils.Utils;

import java.util.List;

public class AdapterCart extends RecyclerView.Adapter<AdapterCart.CartViewHolder> {
    Context mContext;
    List<Cart> mList;

    public AdapterCart(Context ctx,List<Cart> mList) {
        this.mList = mList;
        this.mContext=ctx;
    }

    @NonNull
    @Override
    public CartViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.ada_cart,parent,false);
        return new CartViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull CartViewHolder holder, int position) {
        holder.onBind(mList.get(position));
        Log.e("CART",""+mList.get(position).getImage()+" "+mList.get(position).getProductName());
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class CartViewHolder extends RecyclerView.ViewHolder {
        ImageView imgProduct;
        TextView txtName;
        TextView txtOPrice;
        TextView txtAPrice;
        TextView txtTotal;

        public CartViewHolder(View itemView) {
            super(itemView);
            imgProduct=itemView.findViewById(R.id.adaImgCartProduct);
            txtName=itemView.findViewById(R.id.adaTxtCartPName);
            txtOPrice=itemView.findViewById(R.id.adaTxtCartOfferPrice);
            txtAPrice=itemView.findViewById(R.id.adaTxtCartActualPrice);
            txtTotal=itemView.findViewById(R.id.adaTxtCartTotal);
        }
        private void onBind(Cart cart){
            Glide.with(mContext).load(Integer.parseInt(cart.getImage())).into(imgProduct);
            txtName.setText(cart.getProductName());
            txtOPrice.setText(cart.getOfferPrice());
            txtAPrice.setText("Count : "+cart.getCount());
            txtTotal.setText("Total : "+ Utils.getPrice(cart));

        }
    }
}

package com.samboy.ecommerce.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.samboy.ecommerce.R;
import com.samboy.ecommerce.interfaces.StoreSelect;
import com.samboy.ecommerce.model.Store;

import java.util.List;

public class AdapterStore extends RecyclerView.Adapter<AdapterStore.StoreHolder> {

    List<Store> mStoreList;
    StoreSelect mSelect;

    public AdapterStore(List<Store> mStoreList, StoreSelect select) {
        this.mStoreList = mStoreList;
        this.mSelect=select;
    }

    @NonNull
    @Override
    public StoreHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.ada_store,parent,false);
        return new StoreHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull StoreHolder holder, int position) {
        Store sm=mStoreList.get(position);
        holder.txtSroteName.setText(sm.getName());
        holder.itemView.setOnClickListener(view -> {
            mSelect.onStoreSelect(sm);
        });
    }

    @Override
    public int getItemCount() {
        return mStoreList.size();
    }

    class StoreHolder extends RecyclerView.ViewHolder{
        TextView txtSroteName;
        CardView cardStore;

        public StoreHolder(View itemView) {
            super(itemView);
            txtSroteName=itemView.findViewById(R.id.txtAdaStoreName);
            cardStore=itemView.findViewById(R.id.cardStore);

        }
    }
}

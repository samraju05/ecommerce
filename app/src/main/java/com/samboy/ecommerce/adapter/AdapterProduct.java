package com.samboy.ecommerce.adapter;

import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.samboy.ecommerce.R;
import com.samboy.ecommerce.interfaces.CartInterface;
import com.samboy.ecommerce.model.Product;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AdapterProduct extends RecyclerView.Adapter<AdapterProduct.ProductHolder> {
    List<Product> mList;
    CartInterface mCart;

    private Map<Integer,String> mCartList;

    public AdapterProduct(List<Product> mList,CartInterface mCart) {
        this.mList = mList;
        this.mCart=mCart;
        mCartList=new HashMap<>();
    }

    public void setmCartList(Map<Integer, String> mCartList) {
        this.mCartList = mCartList;
    }
    public void addCartList(int id,String value){
        mCartList.put(id,value);
    }

    @NonNull
    @Override
    public ProductHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.ada_product, parent, false);
        return new ProductHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ProductHolder holder, int position) {
        Product product=mList.get(holder.getAdapterPosition());
        holder.onBind(product);
        holder.btnAddCart.setOnClickListener(view -> {
            holder.btnVisible(1);
            holder.btnIncDec.setNumber("1");
            mCart.onAddToCart(product,1);
        });
        holder.btnIncDec.setOnValueChangeListener((view, oldValue, newValue) -> {
            holder.btnVisible(newValue);
            mCart.onAddToCart(product,newValue);
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class ProductHolder extends RecyclerView.ViewHolder {
        TextView txtOffer;
        TextView txtActualPrice;
        TextView txtOfferPrice;
        TextView txtItemName;
        ImageView imgItem;
        Button btnAddCart;
        ElegantNumberButton btnIncDec;

        public ProductHolder(View itemView) {
            super(itemView);
            btnIncDec = itemView.findViewById(R.id.adaBtnProductIncDec);
            txtOffer = itemView.findViewById(R.id.adaTxtProductOff);
            txtActualPrice = itemView.findViewById(R.id.adaTxtProductActualPrice);
            txtOfferPrice = itemView.findViewById(R.id.adaTxtProductOfferPrice);
            txtItemName = itemView.findViewById(R.id.adaTxtProductName);
            imgItem = itemView.findViewById(R.id.adaImgProductItem);
            btnAddCart = itemView.findViewById(R.id.adaBtnProductAddCart);
            txtActualPrice.setPaintFlags(txtActualPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }
        private void onBind(Product product){
            Glide.with(itemView.getContext()).load(Integer.parseInt(product.getImage())).into(imgItem);
            txtItemName.setText(product.getName());
            onCartAssign(product);
        }

        public void btnVisible(int cartCount) {
            if (cartCount == 0) {
                btnIncDec.setVisibility(View.GONE);
                btnAddCart.setVisibility(View.VISIBLE);
            } else {
                btnIncDec.setVisibility(View.VISIBLE);
                btnAddCart.setVisibility(View.GONE);
            }
        }
        public void onCartAssign(Product product){
            if (mCartList.get(product.getId())!=null){
                btnVisible(1);
                btnIncDec.setNumber(mCartList.get(product.getId()));
            }else {
                btnVisible(0);
            }
        }
    }
}

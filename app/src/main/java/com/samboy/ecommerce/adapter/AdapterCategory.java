package com.samboy.ecommerce.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.samboy.ecommerce.R;
import com.samboy.ecommerce.interfaces.CategoryInterface;
import com.samboy.ecommerce.model.Category;

import java.util.List;

import static android.support.constraint.Constraints.TAG;

public class AdapterCategory extends RecyclerView.Adapter<AdapterCategory.CategoryViewHolder> {
    Context mContext;
    CategoryInterface mCategory;
    List<Category> mList;

    public AdapterCategory(Context mContext, CategoryInterface mCategory, List<Category> mList) {
        this.mContext = mContext;
        this.mCategory = mCategory;
        this.mList = mList;
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.ada_category,parent,false);
        return new CategoryViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {
        holder.onBind(mList.get(position));
        holder.itemView.setOnClickListener(v -> {
            mCategory.onCategoryClick(mList.get(holder.getAdapterPosition()));
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    class CategoryViewHolder extends RecyclerView.ViewHolder{
        ImageView imgCategory;
        TextView txtCategory;
        public CategoryViewHolder(View itemView) {
            super(itemView);
            imgCategory=itemView.findViewById(R.id.adaImgCategory);
            txtCategory=itemView.findViewById(R.id.adaTxtCategoryName);
        }
        public void onBind(Category category){
            Glide.with(mContext).load(Integer.parseInt(category.getImage())).into(imgCategory);
            txtCategory.setText(category.getName());
        }
    }
}

package com.samboy.ecommerce.checkouts;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.samboy.ecommerce.BaseActivity;
import com.samboy.ecommerce.R;
import com.samboy.ecommerce.adapter.AdapterCart;
import com.samboy.ecommerce.app.App;
import com.samboy.ecommerce.constant.IC;
import com.samboy.ecommerce.database.Database;
import com.samboy.ecommerce.model.Cart;
import com.samboy.ecommerce.utils.Utils;

import java.util.List;

public class CartActivity extends BaseActivity implements View.OnClickListener {

    private Toolbar mToolbar;
    private RelativeLayout lyCart, lyCartEmpty;
    private Button btnPurchase;
    private Database db;
    private RecyclerView rvCart;
    private AdapterCart mAdapter;
    private Button btnCheckOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        db = Database.getInstance(this);
        setUpToolBar();
        intViews();
        setListener();
        checkCart(db.getCartCount());
        setSingleRv();
    }

    public void setUpToolBar() {
        mToolbar = findViewById(R.id.toolbarCart);
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_home_back);
        actionBar.setTitle("Cart");
        App.changeToolbarFont(mToolbar);
    }

    public void intViews() {
        btnPurchase = findViewById(R.id.btnEmptyCartPurchase);
        lyCart = findViewById(R.id.ly_cart);
        lyCartEmpty = findViewById(R.id.ly_cart_empty);

        rvCart = findViewById(R.id.rvCart);
        btnCheckOut = findViewById(R.id.btnCartCheckOut);
        setToatalValue();
    }

    public void setListener() {
        btnCheckOut.setOnClickListener(this);
        btnPurchase.setOnClickListener(this);
    }

    private void checkCart(int count) {
        if (count == 0) {
            lyCartEmpty.setVisibility(View.VISIBLE);
            lyCart.setVisibility(View.GONE);
        } else {
            lyCart.setVisibility(View.VISIBLE);
            lyCartEmpty.setVisibility(View.GONE);
        }
    }
    private void setToatalValue(){
        List<Cart> mList=db.getCartItem();
        int price=0;
        for (Cart c:mList){
            price=price+Utils.getPrice(c);
        }
        btnCheckOut.setText("Check Out for Rs "+price);
    }

    private void setSingleRv() {
        mAdapter = new AdapterCart(this,db.getCartItem());
        rvCart.setLayoutManager(Utils.getLyMgr(1, IC.VERTICAL));
        rvCart.setAdapter(mAdapter);
    }


    @Override
    protected void onResume() {
        super.onResume();
        Log.e("onResume"," Cart "+db.isOrderPlaced());
        if (db.isOrderPlaced()){
            finish();
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnEmptyCartPurchase:
                if (!isFinishing()) {
                    finish();
                }
                break;
            case R.id.btnCartCheckOut:
                Intent in=new Intent(this,CheckOutActivity.class);
                startActivity(in);
                break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (!isFinishing()) {
                    finish();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}

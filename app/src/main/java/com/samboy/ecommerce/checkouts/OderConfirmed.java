package com.samboy.ecommerce.checkouts;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.samboy.ecommerce.BaseActivity;
import com.samboy.ecommerce.R;
import com.samboy.ecommerce.app.App;

public class OderConfirmed extends BaseActivity implements View.OnClickListener {
    private Toolbar mToolbar;
    private Button btnStatus;
    private Button btnContinue;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_oder_confirmed);
        setUpToolBar();
        intViews();
        setListener();
    }
    public void setUpToolBar() {
        mToolbar = findViewById(R.id.toolbarOrderConfirm);
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_home_back);
        App.changeToolbarFont(mToolbar);
        actionBar.setTitle("Check Out");
    }
    private void intViews(){
        btnStatus=findViewById(R.id.btnOrderStatus);
        btnContinue=findViewById(R.id.btnOrderContinue);
    }
    private void setListener(){
        btnStatus.setOnClickListener(this);
        btnContinue.setOnClickListener(this);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (!isFinishing()) {
                    finish();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnOrderStatus:
                Toast.makeText(this, "Status", Toast.LENGTH_SHORT).show();
                break;

            case R.id.btnOrderContinue:
                if (!isFinishing()) {
                    finish();
                }
                break;
        }
    }
}

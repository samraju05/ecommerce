package com.samboy.ecommerce.checkouts;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.samboy.ecommerce.BaseActivity;
import com.samboy.ecommerce.R;
import com.samboy.ecommerce.app.App;
import com.samboy.ecommerce.database.Database;

public class CheckOutActivity extends BaseActivity implements View.OnClickListener {
    private Toolbar mToolbar;
    private Database db;

    private TextView txtUserName;
    private TextView txtUserAddress1;
    private TextView txtUserAddress2;
    private TextView txtUserPhoneNumber;
    private TextView txtShipName;
    private TextView txtShipAddress;
    private TextView txtShipPhoneNumber;
    private TextView txtSubTotal;
    private TextView txtSubTax;
    private TextView txtSubShipping;
    private TextView txtSubDiscount;
    private TextView txtTotalPrice;
    private EditText edtCoupon;
    private Button btnCoupon;
    private Button btnCheckOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out);
        db=Database.getInstance(this);
        setUpToolBar();
        intViews();
        setListener();
        setValue();
    }

    public void setUpToolBar() {
        mToolbar = findViewById(R.id.toolbarCheckOut);
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_home_back);
        App.changeToolbarFont(mToolbar);
        actionBar.setTitle("Check Out");
    }

    private void intViews() {
        txtUserName=findViewById(R.id.txtCOUserName);
        txtUserAddress1=findViewById(R.id.txtCOUserAddress1);
        txtUserAddress2=findViewById(R.id.txtCOUserAddress2);
        txtUserPhoneNumber=findViewById(R.id.txtCOUserPhoneNumber);
        txtShipName=findViewById(R.id.txtCOShipName);
        txtShipAddress=findViewById(R.id.txtCOShipAddress);
        txtShipPhoneNumber=findViewById(R.id.txtCOShipPhoneNumber);
        txtSubTotal=findViewById(R.id.txtCOSubTotal);
        txtSubTax=findViewById(R.id.txtCOTax);
        txtSubShipping=findViewById(R.id.txtCOShipping);
        txtSubDiscount=findViewById(R.id.txtCODiscount);
        txtTotalPrice=findViewById(R.id.txtCOTotal);
        edtCoupon=findViewById(R.id.edtCOCouponCode);
        btnCoupon=findViewById(R.id.btnCOApplyCoupon);
        btnCheckOut = findViewById(R.id.btnCheckOutOrder);
    }
    private void setValue(){
        txtShipName.setText(db.getSelectedStore().getName());
    }

    private void setListener() {
        btnCoupon.setOnClickListener(this);
        btnCheckOut.setOnClickListener(this);
    }
    @Override
    protected void onResume() {
        super.onResume();
        if (db.isOrderPlaced()){
            finish();
        }
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnCheckOutOrder:
                db.setOderPlaced(true);
                db.clearCart();
                startActivity(new Intent(this, OderConfirmed.class));
                break;

            case R.id.btnCOApplyCoupon:

                break;
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (!isFinishing()) {
                    finish();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}

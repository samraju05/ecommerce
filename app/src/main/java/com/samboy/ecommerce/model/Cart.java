package com.samboy.ecommerce.model;

/**
 * Created by Hari Group Unity on 26-03-2018.
 */

public class Cart {
    int id;
    String productName;
    String image;
    String actualPrice;
    String offerPrice;
    int count;

    public Cart() {
    }

    public Cart(int id, String productName, String image, String actualPrice, String offerPrice, int count) {
        this.id = id;
        this.productName = productName;
        this.image = image;
        this.actualPrice = actualPrice;
        this.offerPrice = offerPrice;
        this.count = count;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getActualPrice() {
        return actualPrice;
    }

    public void setActualPrice(String actualPrice) {
        this.actualPrice = actualPrice;
    }

    public String getOfferPrice() {
        return offerPrice;
    }

    public void setOfferPrice(String offerPrice) {
        this.offerPrice = offerPrice;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}

package com.samboy.ecommerce.model;

public class Product {
    int id;
    String name;
    String image;
    String actualPrice;
    String offerPrice;

    public Product() {
    }

    public Product(int id, String name, String image, String actualPrice, String offerPrice) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.actualPrice = actualPrice;
        this.offerPrice = offerPrice;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getActualPrice() {
        return actualPrice;
    }

    public void setActualPrice(String actualPrice) {
        this.actualPrice = actualPrice;
    }

    public String getOfferPrice() {
        return offerPrice;
    }

    public void setOfferPrice(String offerPrice) {
        this.offerPrice = offerPrice;
    }
}

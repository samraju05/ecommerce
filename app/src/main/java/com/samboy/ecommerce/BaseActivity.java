package com.samboy.ecommerce;

import android.os.Bundle;
import android.os.SystemClock;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

public class BaseActivity  extends AppCompatActivity {
    int onStartCount = 0;
    private long mLastBackPressed = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        onStartCount = 1;
        if (savedInstanceState == null) // 1st time
        {
            this.overridePendingTransition(R.anim.anim_slide_in_left,
                    R.anim.anim_slide_out_left);
        } else // already created so reverse animation
        {
            onStartCount = 2;
        }
    }
    public void showToast(String msg){
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }
    public void showSnackbar(View v,String msg){
        Snackbar.make(v,msg,Snackbar.LENGTH_LONG).show();
    }

    public void doublClickExit(){
        if (SystemClock.elapsedRealtime() < (mLastBackPressed + 2000)) {
            finish();
            return;
        }
        mLastBackPressed = SystemClock.elapsedRealtime();
        Toast.makeText(this, R.string.msg_press_back_again, Toast.LENGTH_SHORT).show();

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (onStartCount > 1) {
            this.overridePendingTransition(R.anim.anim_slide_in_right,
                    R.anim.anim_slide_out_right);

        } else if (onStartCount == 1) {
            onStartCount++;
        }

    }
}

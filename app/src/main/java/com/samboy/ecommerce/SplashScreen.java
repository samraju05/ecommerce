package com.samboy.ecommerce;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.samboy.ecommerce.database.Database;
import com.samboy.ecommerce.location.SelectLocation;

public class SplashScreen extends BaseActivity {
    private static final int TIMER_DURATION = 2000;
    private Database db;
    private ImageView imgLoad;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= 21) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        setContentView(R.layout.activity_splash_screen);
        db = Database.getInstance(this);
        imgLoad = findViewById(R.id.imgLoad);
        startTimer();
        Glide.with(this)
                .asGif()
                .load(R.drawable.loader)
                .into(imgLoad);

    }

    private void startTimer() {
        new Handler().postDelayed(() -> {
            gotoAct();
        }, TIMER_DURATION);
    }

    private void gotoAct() {
        if (db.isHelpFirstTime()){
            if (!isFinishing()) {
                startActivity(new Intent(this, HelpScreen.class));
                finish();
            }
        }else if (!db.isSelectedStore()){
            if (!isFinishing()) {
                startActivity(new Intent(this, SelectLocation.class));
                finish();
            }
        }else {
            if (!isFinishing()) {
                startActivity(new Intent(this, HomeActivity.class));
                finish();
            }
        }

    }
}


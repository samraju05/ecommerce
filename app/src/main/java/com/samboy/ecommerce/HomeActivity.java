package com.samboy.ecommerce;

import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.samboy.ecommerce.adapter.AdapterProduct;
import com.samboy.ecommerce.checkouts.CartActivity;
import com.samboy.ecommerce.constant.IC;
import com.samboy.ecommerce.database.Database;
import com.samboy.ecommerce.interfaces.CartInterface;
import com.samboy.ecommerce.location.SelectStore;
import com.samboy.ecommerce.model.Cart;
import com.samboy.ecommerce.model.Product;
import com.samboy.ecommerce.ui.NotificationDrawable;
import com.samboy.ecommerce.utils.DummyArrays;
import com.samboy.ecommerce.utils.NetworkStateReceiver;
import com.samboy.ecommerce.constant.SC;
import com.samboy.ecommerce.utils.Utils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HomeActivity extends BaseActivity implements View.OnClickListener, NetworkStateReceiver.NetworkStateReceiverListener, SwipeRefreshLayout.OnRefreshListener, CartInterface {
    private Toolbar mToolbar;
    private TextView txtStore;
    private TextView txtStoreChange;
    private TextView txtCategory;
    private RecyclerView rvProduct;

    private RelativeLayout lyRoot;
    private SwipeRefreshLayout mSwip;
    private LinearLayout lyNoNet, lyHome;


    private Menu menu;
    private LayerDrawable notificationDrawable;
    private NotificationDrawable badge;
    private MenuItem itemNotify;

    private Database db;
    private AdapterProduct mAdapter;
    private NetworkStateReceiver networkStateReceiver;
    private Map<Integer,String> mCartList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        db = Database.getInstance(this);
        mCartList=new HashMap<>();
        setUpToolBar();
        intViews();
        setListener();
        intNetWorkReceiver();

    }

    public void setUpToolBar() {
        mToolbar = findViewById(R.id.toolbarHome);
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("");
    }

    private void intViews() {
        lyRoot = findViewById(R.id.lyRoot);
        lyNoNet = findViewById(R.id.lyNoNetwork);
        lyHome = findViewById(R.id.lyHome);
        mSwip=findViewById(R.id.swipeHome);
        txtStore = findViewById(R.id.txtHomeStore);
        txtStoreChange = findViewById(R.id.txtHomeStoreChange);
        txtCategory = findViewById(R.id.txtHomeCategory);
        rvProduct=findViewById(R.id.rvProduct);

    }

    private void intNetWorkReceiver() {
        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));
    }

    private void setValue() {
        txtStore.setText(db.getSelectedStore().getName());
        db.setOderPlaced(false);
        if (db.isSelectedCategory()){
            if (db.getSelectedCategory().getName().equals("All")){
                txtCategory.setText("CATEGORY");
            }else {
                txtCategory.setText(db.getSelectedCategory().getName());
            }
            setRvProduct();
        }else {
            setRvProduct();
        }
        setMenuCart(db.getCartCount());
    }

    private void setListener() {
        txtStoreChange.setOnClickListener(this);
        txtCategory.setOnClickListener(this);
        mSwip.setOnRefreshListener(this);
        
    }
    private void setRvProduct(){
        int catId=1;
        if (db.getSelectedCategory()!=null)catId=db.getSelectedCategory().getId();
        mAdapter=new AdapterProduct(DummyArrays.getProducts(catId),this);
        mAdapter.setmCartList(mCartList);
        rvProduct.setLayoutManager(Utils.getLyMgr(2, IC.VERTICAL));
        rvProduct.setAdapter(mAdapter);
    }

    private void setCartList(){
        mCartList.clear();
        List<Cart> mList=db.getCartItem();
        for (Cart cm:mList){
            mCartList.put(cm.getId(),String.valueOf(cm.getCount()));
        }
    }

    private void checkNetwork(boolean isNetwork) {
        lyNoNet.setVisibility(isNetwork?View.GONE:View.VISIBLE);
        lyHome.setVisibility(isNetwork?View.VISIBLE:View.GONE);
    }

    @Override
    protected void onResume() {
        setCartList();
        setValue();
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        networkStateReceiver.removeListener(this);
        this.unregisterReceiver(networkStateReceiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        itemNotify = menu.findItem(R.id.menuHomeCart);
        notificationDrawable = (LayerDrawable) itemNotify.getIcon();
        setMenuCart(db.getCartCount());
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuHomeCart:
                Intent in = new Intent(this, CartActivity.class);
                startActivity(in);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setMenuCart(int count) {
        if (count < 0 || notificationDrawable == null || itemNotify == null) {
            return;
        }
        // Reuse drawable if possible
        Drawable reuse = notificationDrawable.findDrawableByLayerId(R.id.ic_menu_cart);
        if (reuse != null && reuse instanceof NotificationDrawable) {
            badge = (NotificationDrawable) reuse;
        } else {
            badge = new NotificationDrawable(getApplicationContext());
        }
        badge.setCount(String.valueOf(count));
        notificationDrawable.mutate();
        boolean set = notificationDrawable.setDrawableByLayerId(R.id.ic_menu_cart, badge);
        itemNotify.setIcon(notificationDrawable);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txtHomeStoreChange:
                Intent in = new Intent(this, SelectStore.class);
                in.putExtra(SC.ACTIVITY, IC.FINISH_STORE);
                startActivity(in);
                break;

            case R.id.txtHomeCategory:
                Intent inc = new Intent(this, CategoryActivity.class);
                startActivity(inc);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        doublClickExit();
    }

    @Override
    public void networkAvailable() {
        checkNetwork(true);

    }

    @Override
    public void networkUnavailable() {
        checkNetwork(false);
    }

    @Override
    public void onRefresh() {
        mSwip.setRefreshing(false);
    }

    @Override
    public void onAddToCart(Product product, int count) {
        if (count==0){
            db.deleteCartItem(product.getId());
            setMenuCart(db.getCartCount());
            return;
        }
        db.addCart(new Cart(product.getId(),product.getName(),product.getImage(),product.getActualPrice(),product.getOfferPrice(),count));
        mAdapter.addCartList(product.getId(), String.valueOf(count));
        mCartList.put(product.getId(),String.valueOf(count));
        setMenuCart(db.getCartCount());
    }
}

package com.samboy.ecommerce.app;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.design.widget.NavigationView;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.style.TypefaceSpan;
import android.util.AttributeSet;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.TextView;

import com.samboy.ecommerce.R;


public class App extends Application {


    @Override
    public void onCreate() {
        super.onCreate();
    }

    public static void setTypeface(TextView view, AttributeSet attrs) {
        Context ctx = view.getContext();
        if (attrs != null) {
            try {
                String font = null;
                for (int i = 0, j = attrs.getAttributeCount(); i < j; i++) {
                    if (attrs.getAttributeName(i).equals("font")) {
                        font = ctx.getString(attrs.getAttributeResourceValue(i, R.string.font_helvetica_light));
                    }
                }
                if (font == null) {
                    view.setTypeface(Typeface.createFromAsset(ctx.getAssets(), ctx.getString(R.string.font_helvetica_light)));
                } else {
                    view.setTypeface(Typeface.createFromAsset(view.getContext().getAssets(), font));
                }

            } catch (Exception e) {
                view.setTypeface(Typeface.createFromAsset(ctx.getAssets(), ctx.getString(R.string.font_helvetica_light)));
            }
        } else {
            view.setTypeface(Typeface.createFromAsset(ctx.getAssets(), ctx.getString(R.string.font_helvetica_light)));
        }
    }
    public static void changeToolbarFont(Toolbar toolbar) {
        for (int i = 0; i < toolbar.getChildCount(); i++) {
            View view = toolbar.getChildAt(i);
            if (view instanceof TextView) {
                TextView tv = (TextView) view;
                if (tv.getText().equals(toolbar.getTitle())) {
                    setTypeface(tv,null);
                    break;
                }
            }
        }
    }

    //APPLY FONT TO NAVIGATION MENU
    public static void customMenuFont(Context context, NavigationView mNavigationView) {
        Typeface font = Typeface.createFromAsset(context.getAssets(), context.getString(R.string.font_helvetica_light));
        Menu menu = mNavigationView.getMenu();
        for (int i = 0; i < menu.size(); i++) {
            MenuItem mi = menu.getItem(i);
            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem, font);
                }
            }
            //the method we have create in activity
            applyFontToMenuItem(mi, font);
        }
    }

    //APPLY MENU FONT TO MENU ITEM
    private static void applyFontToMenuItem(MenuItem mi, Typeface font) {

        // Typeface custom_font = Typeface.createFromAsset(getApplicationContext().getAssets(),  "fonts/HELVETICANEUE-LIGHT.OTF");
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }


    static class CustomTypefaceSpan extends TypefaceSpan {

        private final Typeface newType;

        public CustomTypefaceSpan(String family, Typeface type) {
            super(family);
            newType = type;
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            applyCustomTypeFace(ds, newType);
        }

        @Override
        public void updateMeasureState(TextPaint paint) {
            applyCustomTypeFace(paint, newType);
        }

        private static void applyCustomTypeFace(Paint paint, Typeface tf) {
            int oldStyle;
            Typeface old = paint.getTypeface();
            if (old == null) {
                oldStyle = 0;
            } else {
                oldStyle = old.getStyle();
            }

            int fake = oldStyle & ~tf.getStyle();
            if ((fake & Typeface.BOLD) != 0) {
                paint.setFakeBoldText(true);
            }

            if ((fake & Typeface.ITALIC) != 0) {
                paint.setTextSkewX(-0.25f);
            }

            paint.setTypeface(tf);
        }
    }

}

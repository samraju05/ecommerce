package com.samboy.ecommerce.interfaces;

import com.samboy.ecommerce.model.Store;

public interface StoreSelect {
    void onStoreSelect(Store storeModel);
}

package com.samboy.ecommerce.interfaces;

import com.samboy.ecommerce.model.Category;

public interface CategoryInterface {
    void onCategoryClick(Category category);
}

package com.samboy.ecommerce.interfaces;

import com.samboy.ecommerce.model.Product;

/**
 * Created by Hari Group Unity on 26-03-2018.
 */

public interface CartInterface {
    void onAddToCart(Product product, int count);
}

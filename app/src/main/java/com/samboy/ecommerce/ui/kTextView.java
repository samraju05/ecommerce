package com.samboy.ecommerce.ui;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.samboy.ecommerce.app.App;

/**
 * Created by Hari Group Unity on 26-02-2018.
 */

public class kTextView extends AppCompatTextView {

    public kTextView(Context context) {
        super(context);
        init(null);
    }

    public kTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public kTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        App.setTypeface(this, attrs);

    }
}

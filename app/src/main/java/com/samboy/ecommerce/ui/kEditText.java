package com.samboy.ecommerce.ui;

import android.content.Context;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

import com.samboy.ecommerce.app.App;


/**
 * Created by Hari Group Unity on 26-02-2018.
 */

public class kEditText extends AppCompatEditText {

    public kEditText(Context context) {
        super(context);
        init(null);
    }

    public kEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public kEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }
    private void init(AttributeSet attrs) {
        App.setTypeface(this, attrs);

    }
}


package com.samboy.ecommerce.ui.Expandble;

public interface OnExpandListener {

    public void onExpanded(ExpandableLayout view);

    public void onCollapsed(ExpandableLayout view);
}

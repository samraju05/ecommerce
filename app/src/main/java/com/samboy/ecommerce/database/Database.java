package com.samboy.ecommerce.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.samboy.ecommerce.model.Cart;
import com.samboy.ecommerce.model.Category;
import com.samboy.ecommerce.model.Product;
import com.samboy.ecommerce.model.Store;
import com.samboy.ecommerce.model.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Hari Group Unity on 26-03-2018.
 */

public class Database {

    private static final String TBL_SELECTED_STORE = "tbl_selected_store";
    private static final String TBL_SELECTED_CATE = "tbl_selected_cate";
    private static final String TBL_GENERAL = "tbl_general";
    private static final String TBL_CART = "tbl_cart";
    private static final String TBL_USER = "tbl_user";

    private static final String CLM_ID = "id";
    private static final String CLM_STORE = "store";
    private static final String CLM_NAME = "name";
    private static final String CLM_CATEGORY = "category";
    private static final String CLM_PRODUCT_NAME = "product_name";
    private static final String CLM_PRICE = "price";
    private static final String CLM_IMAGE = "image";
    private static final String CLM_COUNT = "count";
    private static final String CLM_EMAIL = "email";
    private static final String CLM_PHONENUMBER = "phone_number";
    private static final String CLM_ADDRESS = "address";
    private static final String CLM_CITY = "city";
    private static final String CLM_PIN = "pincode";



    private static final String CLM_IS_TRUE = "is_true";
    private static final String CLM_CONTENT = "clm_content";
    private static final String CLM_KEY = "clmKey";
    private static final String KEY_HELP_FIRST_LAUNCH = "help_first";
    private static final String KEY_ORDER_PLACED = "order_placed";

    private SQLiteDatabase db;
    private MyDatabase openHelper;
    private static Database database;

    private Database(Context ctx) {
        openHelper = new MyDatabase(ctx);
        db = openHelper.getWritableDatabase();
    }

    public static Database init(Context ctx) {
        //if (database == null) {
        database = new Database(ctx);
        // }
        return database;
    }

    public static Database getInstance(Context ctx) {
        if (database == null) {
            database = new Database(ctx);
        }
        return database;
    }

    public static Database getInstance() {
        if (database != null) {
            database.openHelper.onCreate(database.openHelper.getWritableDatabase());
        }
        return database;
    }

    //To check data exists or not
    public static void checkData() {
        if (database != null) {
            database.openHelper.onCreate(database.openHelper.getWritableDatabase());
            database.db = database.openHelper.getWritableDatabase();
        }
    }

    public static void clear() {
        if (database != null) {
            try {
                database.openHelper.close();
            } catch (Exception e) {
            }
            try {
                database.db.close();
            } catch (Exception e) {
            }
            database.openHelper = null;
            database.db = null;
        }
        database = null;

    }

    public static void closeCursor(Cursor cursor) {
        try {
            cursor.close();
        } catch (Exception e) {
        }
    }

    private void delete(String table) {
        delete(table, null, null);
    }

    private void delete(String table, String condition, String[] args) {
        db.delete(table, condition, args);
    }

    /*********************SELECTED STORE**************************/

    public void addSelectedStore(Store sm) {
        Log.e("DB","store add "+sm.getName());
        delete(TBL_SELECTED_STORE);
        db.insert(TBL_SELECTED_STORE,null,getCvForStore(sm));
    }

    public Store getSelectedStore() {
        Cursor c = db.query(TBL_SELECTED_STORE, null, null, null, null, null, null);
        if (c.getCount() == 0) {
            return null;
        }
        Store sm=new Store();
        while (c.moveToFirst()) {
            sm.setId(c.getInt(0));
            sm.setName(c.getString(1));
            return sm;
        }
        closeCursor(c);
        return sm;
    }

    public boolean isSelectedStore() {
        return getSelectedStoreId(-1) > 0;
    }

    private int getSelectedStoreId(int defaultId) {
        Store model = getSelectedStore();
        if (model != null) {
            return model.getId();
        }
        return defaultId;
    }

    /*********************SELECTED CATE**************************/

    public void addSelectedCategory(Category category) {
        Log.e("DB","category add "+category.getName());
        delete(TBL_SELECTED_CATE);
        db.insert(TBL_SELECTED_CATE,null,getCvForCategory(category));
    }

    public Category getSelectedCategory() {
        Cursor c = db.query(TBL_SELECTED_CATE, null, null, null, null, null, null);
        if (c.getCount() == 0) {
            return null;
        }
        Category category=new Category();
        while (c.moveToFirst()) {
            category.setId(c.getInt(0));
            category.setName(c.getString(1));
            category.setImage(c.getString(2));
            return category;
        }
        closeCursor(c);
        return category;
    }

    public boolean isSelectedCategory() {
        return getSelectedCategoryId(-1) > 0;
    }

    private int getSelectedCategoryId(int defaultId) {
        Category model = getSelectedCategory();
        if (model != null) {
            return model.getId();
        }
        return defaultId;
    }
    /*****************************USER*********************************/

    public void loginUser(User user){
        delete(TBL_USER);
        db.insert(TBL_USER,null,getCvForUser(user));
    }
    public void logoutUser(){
        delete(TBL_USER);
    }
    public boolean isUserLoggedIn(){
        Cursor c=db.query(TBL_USER,null,null,null,null,null,null);
        if (c.getCount()!=0){
            return true;
        }
        closeCursor(c);
        return false;
    }
    public User getLoggedUser(){
        Cursor c=db.query(TBL_USER,null,null,null,null,null,null);
        User user=new User();
        while (c.moveToFirst()){
            user.setId(c.getInt(0));
            user.setName(c.getString(1));
            user.setEmail(c.getString(2));
            user.setPhoneNumber(c.getString(3));
            user.setAddress(c.getString(4));
            user.setCity(c.getString(5));
            user.setPincode(c.getString(6));
        }
        closeCursor(c);
        return user;
    }


    /*****************************CART********************************/

    public void addCart(Cart cm) {

        int count= getItemCount(cm.getId());
        if (count!=0){
            updateCart(cm);
        }else {
            db.insert(TBL_CART, null, getCvForCart(cm));
        }

    }
    public void updateCart(Cart cm) {
        ContentValues cv = getCvForCart(cm);
        cv.remove(CLM_ID);
        db.update(TBL_CART, cv, String.format("%s=?", CLM_ID), new String[]{String.valueOf(cm.getId())});
    }

    public void deleteCartItem(int itemId){
        db.delete(TBL_CART,  String.format("%s=?", CLM_ID), new String[]{String.valueOf(itemId)});
    }

    public int getCartCount() {
        Cursor cursor = db.query(TBL_CART, null,null,null,null,null,null);
        int count = cursor.getCount();
        closeCursor(cursor);
        return count;
    }
    private int getItemCount(int itemId) {
        int itemCount = 0;
//        Cursor mCursor = db.rawQuery("SELECT " + CLM_COUNT + "  FROM " + TBL_CART + " WHERE " + CLM_ID + "= '" + itemId + "'", null);
        Cursor mCursor = db.rawQuery(String.format("SELECT %s FROM %s WHERE %s='%s'",CLM_COUNT,TBL_CART,CLM_ID,itemId), null);

        if (mCursor == null) {
            return itemCount;
        }
        while (mCursor.moveToFirst()) {
            itemCount = mCursor.getInt(mCursor.getColumnIndex(CLM_COUNT));
            return itemCount;
        }
        closeCursor(mCursor);
        return itemCount;
    }

    public List<Cart> getCartItem() {
        List<Cart> mList = new ArrayList<>();
        Cursor c = db.query(TBL_CART, null, null, null, null, null, null);
        while (c.moveToNext()) {
            Cart model = new Cart();
            model.setId(c.getInt(0));
            model.setProductName(c.getString(1));
            model.setImage(c.getString(2));
            model.setOfferPrice(c.getString(3));
            model.setCount(c.getInt(4));
            mList.add(model);
        }
        closeCursor(c);
        return mList;
    }

    public void clearCart() {
        delete(TBL_CART);
    }


    /*****************************HELP SCREEN**************************/

    public boolean isHelpFirstTime() {
        return isTrue(db.query(TBL_GENERAL, new String[]{CLM_IS_TRUE}, String.format("%s=?", CLM_KEY), new String[]{KEY_HELP_FIRST_LAUNCH}, null, null, null));
    }

    public void setHelpFirstTime(boolean on) {
        setOnOff(TBL_GENERAL, CLM_IS_TRUE, KEY_HELP_FIRST_LAUNCH, String.valueOf(on ? 1 : 0));
    }



    /*****************************ORDER PLACED**************************/

    public boolean isOrderPlaced() {
        return isTrue(db.query(TBL_GENERAL, new String[]{CLM_IS_TRUE}, String.format("%s=?", CLM_KEY), new String[]{KEY_ORDER_PLACED}, null, null, null));
    }

    public void setOderPlaced(boolean on) {
        setOnOff(TBL_GENERAL, CLM_IS_TRUE, KEY_ORDER_PLACED, String.valueOf(on ? 1 : 0));
    }

    private boolean isTrue(Cursor c) {
        boolean isTrue = false;
        try {
            if (c.moveToFirst())
                isTrue = c.getInt(0) == 1;
        } catch (Exception e) {
        }
        closeCursor(c);
        return isTrue;
    }
    private void setOnOff(String table, String clm, String key, String value) {
        ContentValues cv = new ContentValues();
        cv.put(clm, value);
        db.update(table, cv, String.format("%s=?", CLM_KEY), new String[]{key});
    }

    /**********************CONTENT VALUES***********************/

    private ContentValues getCvForStore(Store sm) {
        ContentValues cv = new ContentValues();
        cv.put(CLM_ID, sm.getId());
        cv.put(CLM_STORE, sm.getName());
        return cv;
    }
    private ContentValues getCvForProduct(Product product){
        ContentValues cv=new ContentValues();
        cv.put(CLM_ID,product.getId());
        cv.put(CLM_NAME,product.getName());
        return cv;
    }
    private ContentValues getCvForCategory(Category category){
        ContentValues cv=new ContentValues();
        Log.e("DB_C",""+category.getId()+" "+category.getName());
        cv.put(CLM_ID,category.getId());
        cv.put(CLM_CATEGORY,category.getName());
        cv.put(CLM_IMAGE,category.getImage());
        return cv;
    }
    private ContentValues getCvForCart(Cart cm) {
        ContentValues cv = new ContentValues();
        cv.put(CLM_ID, cm.getId());
        cv.put(CLM_PRODUCT_NAME, cm.getProductName());
        cv.put(CLM_IMAGE, cm.getImage());
        cv.put(CLM_PRICE, cm.getOfferPrice());
        cv.put(CLM_COUNT, cm.getCount());
        return cv;
    }
    private ContentValues getCvForUser(User user){
        ContentValues cv=new ContentValues();
        cv.put(CLM_ID,user.getId());
        cv.put(CLM_NAME,user.getName());
        cv.put(CLM_EMAIL,user.getEmail());
        cv.put(CLM_PHONENUMBER,user.getPhoneNumber());
        cv.put(CLM_ADDRESS,user.getAddress());
        cv.put(CLM_CITY,user.getCity());
        cv.put(CLM_PIN,user.getPincode());
        return cv;
    }


    public static class MyDatabase extends SQLiteOpenHelper {

        private Context context;

        public MyDatabase(Context context) {
            super(context, "sqlite.kunnil", null, 1);
            this.context = context;
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            if (context == null) {
                return;
            }
            context = null;
            db.execSQL(gcs(TBL_USER,new String[]{CLM_NAME,CLM_EMAIL,CLM_PHONENUMBER,CLM_ADDRESS,CLM_CITY,CLM_PIN},
                    new int[]{0,0,0,0,0,0}));
            db.execSQL(gcs(TBL_SELECTED_STORE, new String[]{CLM_STORE},
                    new int[]{0}));
            db.execSQL(gcs(TBL_SELECTED_CATE, new String[]{CLM_CATEGORY,CLM_IMAGE},
                    new int[]{0,0}));
            db.execSQL(gcs(TBL_CART, new String[]{CLM_PRODUCT_NAME, CLM_IMAGE, CLM_PRICE,CLM_COUNT},
                    new int[]{0, 0, 0,1}));
            db.execSQL(gcs(TBL_GENERAL,new String[]{CLM_KEY,CLM_IS_TRUE},
                    new int[]{0,1}));


            ContentValues cv=new ContentValues();
            cv.put(CLM_KEY, KEY_HELP_FIRST_LAUNCH);
            cv.put(CLM_IS_TRUE, 1);
            db.insert(TBL_GENERAL, null, cv);

            ContentValues cvOder=new ContentValues();
            cvOder.put(CLM_KEY, KEY_ORDER_PLACED);
            cvOder.put(CLM_IS_TRUE, 0);
            db.insert(TBL_GENERAL, null, cvOder);

        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        }

        private String gcs(String table, String[] clms, int[] type) {

            StringBuilder builder = new StringBuilder();
            builder.append("CREATE TABLE IF NOT EXISTS ").append(table).append(" (").append(CLM_ID).append(" INT(100) ,");
            for (int i = 0, j = clms.length; i < j; i++) {
                builder.append(clms[i]).append(" ").append(getStringForType(type[i])).append(",");
            }
            if (clms.length > 0) {
                builder.deleteCharAt(builder.length() - 1);
                builder.append(")");
            }
            return builder.toString();
        }

        private String getStringForType(int type) {
            switch (type) {
                case 0:
                    return "VARCHAR(255)";
                case 1:
                    return "INT(50)";
                case 2:
                    return "TEXT";
                case 3:
                    return "BIGINT(20)";
                case 4:
                    return "FLOAT";
                case 5:
                    return "INT(1)";
                case 6:
                    return "DATETIME";
            }
            return "VARCHAR(255)";
        }
    }
}
